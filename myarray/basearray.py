from typing import Tuple
from typing import List
from typing import Union
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """

    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0] * n
            elif dtype == float:
                self.__data = [0.0] * n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    # ----------------------------------------------------------------------------
    def __str__(self):
        stringrep = ""
        maxlen = 0
        for v in self:
            if len(str(v)) > maxlen:
                maxlen = len(str(v))
        maxlen += 1
        if len(self.shape) == 1:
            i = 0
            for v in self:
                # print(str(v).rjust(maxlen), end="")
                stringrep += str(v).rjust(maxlen) + ""
                i += 1
            # print(end="\n")
            stringrep += "\n"
        elif len(self.shape) == 2:
            i = 0
            for v in self:
                # print(str(v).rjust(maxlen), end="")
                stringrep += str(v).rjust(maxlen) + ""
                i += 1
                if i % self.shape[1] == 0:
                    # print(end="\n")
                    stringrep += "\n"
        elif len(self.shape) == 3:
            i = 0
            for v in self:
                # print(str(v).rjust(maxlen), end="")
                stringrep += str(v).rjust(maxlen) + ""
                i += 1
                if i % self.shape[1] == 0:
                    # print(end="\n")
                    stringrep += "\n"
                if i % (self.shape[0] * self.shape[1]) == 0:
                    # print(end="\n")
                    if i != (self.shape[0] * self.shape[1] * self.shape[2]):
                        stringrep += "\n"

        return stringrep

    def transpose(self):
        if len(self.shape) != 2:
            return self
        narr = BaseArray((self.shape[1], self.shape[0]), dtype=self.dtype)

        for j in range(0, self.shape[1]):
            for i in range(0, self.shape[0]):
                narr[j, i] = self[i, j]
        return narr

    def _merge_sort_part(self, nlist, lhalf, rhalf):
        i = 0
        j = 0
        k = 0
        while i < len(lhalf) and j < len(rhalf):
            if lhalf[i] < rhalf[j]:
                nlist[k] = lhalf[i]
                i += 1
            else:
                nlist[k] = rhalf[j]
                j += 1
            k += 1

        while i < len(lhalf):
            nlist[k] = lhalf[i]
            i += 1
            k += 1

        while j < len(rhalf):
            nlist[k] = rhalf[j]
            j += 1
            k += 1

    def _merge_sort(self, nlist):
        lhalf = []
        rhalf = []
        if len(nlist) > 1:
            mid = len(nlist) // 2
            lhalf = nlist[:mid]
            rhalf = nlist[mid:]
            self._merge_sort(lhalf)
            self._merge_sort(rhalf)
        self._merge_sort_part(nlist, lhalf, rhalf)

    def _sublist(self, start, end):
        llist = []
        for i in range(start, end):
            llist.append(self._BaseArray__data[i])
        return llist

    def _sublist_insert(self, start, slist):
        i = start
        for v in slist:
            self._BaseArray__data[i] = v
            i += 1
        return self

    # 0 - horizontal, 1 - vertical
    def sort(self, arr, stype):
        warr = arr
        if stype == 1:
            warr = warr.transpose()

        # sort by rows
        if len(warr.shape) == 1:
            warr._merge_sort(warr._BaseArray__data)
        else:
            for i in range(0, warr.shape[0]):
                llist = warr._sublist(i * warr.shape[1], i * warr.shape[1] + warr.shape[1])
                warr._merge_sort(llist)
                warr = warr._sublist_insert(i * warr.shape[1], llist)

        if stype == 1:
            warr = warr.transpose()

        return warr

    def search(self, num):
        results = []
        if len(self.shape) == 1:
            for i, v in enumerate(self):
                if v == num:
                    results.append(i)
        elif len(self.shape) == 2:
            for i, v in enumerate(self):
                if v == num:
                    y = math.floor(i / self.shape[1])
                    x = i % self.shape[1]
                    results.append([y, x])
        elif len(self.shape) == 3:
            for i, v in enumerate(self):
                if v == num:
                    z = math.floor(i / (self.shape[1] * self.shape[0]))
                    y = math.floor((i - (z * self.shape[1] * self.shape[0])) / self.shape[1])
                    x = i % self.shape[1]
                    results.append([y, x, z])
        return results

    def log_(self, sc):
        narr = None

        f = 0
        for i, v in enumerate(self):
            if not math.log(self._BaseArray__data[i], sc).is_integer():
                f = 1
                break

        if f == 1:
            narr = BaseArray(self.shape, dtype=float, data=self._BaseArray__data)
        else:
            narr = BaseArray(self.shape, dtype=int, data=self._BaseArray__data)

        for i, v in enumerate(narr):
            narr._BaseArray__data[i] = math.log(narr._BaseArray__data[i], sc)
        return narr

    def exp_(self, sc):
        narr = None
        if isinstance(sc, float):
            narr = BaseArray(self.shape, dtype=float, data=self._BaseArray__data)
        elif isinstance(sc, int):
            narr = BaseArray(self.shape, self.dtype, data=self._BaseArray__data)
        else:
            return None

        for i, v in enumerate(narr):
            narr._BaseArray__data[i] = math.pow(narr._BaseArray__data[i], sc)
        return narr

    # tabela x tabela
    def __mul__(self, other):
        if not isinstance(other, BaseArray):
            raise Exception('Other is not a matrix')
        elif not self.shape[0] == other.shape[1] and not self.shape[1] == other.shape[0]:
            raise Exception('Cannot multiply matricies with these dimensions')
        elif not len(self.shape) == 2:
            raise Exception('Matrix is not 2D')

        narr = None
        if self.dtype == float or other.dtype == float:
            narr = BaseArray((self.shape[0], other.shape[1]), dtype=float)
        else:
            narr = BaseArray((self.shape[0], other.shape[1]), dtype=int)

        for i in range(0, self.shape[0]):
            for j in range(0, other.shape[1]):
                ssum = 0
                for k in range(0, other.shape[0]):
                    ssum += self[i, k] * other[k, j]
                narr[i, j] = ssum
        return narr

    # skalar
    def __rmul__(self, sc):
        narr = None
        if isinstance(sc, float):
            narr = BaseArray(self.shape, dtype=float, data=self._BaseArray__data)
        elif isinstance(sc, int):
            narr = BaseArray(self.shape, self.dtype, data=self._BaseArray__data)
        else:
            return None

        for i, v in enumerate(narr):
            narr._BaseArray__data[i] *= sc
        return narr

    def __add__(self, other):
        if not isinstance(other, BaseArray):
            raise Exception('Other is not a matrix')
        elif not self.shape == other.shape:
            raise Exception('Matricies have different dimensions')

        narr = None
        if self.dtype == float or other.dtype == float:
            narr = BaseArray(self.shape, dtype=float)
        else:
            narr = BaseArray(self.shape, self.dtype)

        for i, v in enumerate(self.__data):
            narr._BaseArray__data[i] = self.__data[i] + other.__data[i]

        return narr

    def __sub__(self, other):
        if not isinstance(other, BaseArray):
            raise Exception('Other is not a matrix')
        elif not self.shape == other.shape:
            raise Exception('Matricies have different dimensions')

        narr = None
        if self.dtype == float or other.dtype == float:
            narr = BaseArray(self.shape, dtype=float)
        else:
            narr = BaseArray(self.shape, self.dtype)

        for i, v in enumerate(self.__data):
            narr._BaseArray__data[i] = self.__data[i] - other.__data[i]

        return narr
    # ---------------------------------------------------------------------------


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,) + ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n] * s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
