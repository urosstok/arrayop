from unittest import TestCase
from myarray.basearray import BaseArray


class TestFunctions(TestCase):
    def test_transpose(self):
        a = BaseArray((2, 3), dtype=float, data=(-5, -9, -40, 1, 300, 6))
        ret = a.transpose().__str__()
        self.assertEqual("  -5   1\n  -9 300\n -40   6\n", ret)

    def test_print1D(self):
        a = BaseArray((4,), dtype=float, data=(2, 4, 6, 9))
        ret = a.__str__()
        self.assertEqual(" 2 4 6 9\n", ret)

    def test_print2D(self):
        a = BaseArray((2, 3), dtype=float, data=(-5, -9, -40, 1, 300, 6))
        ret = a.__str__()
        self.assertEqual("  -5  -9 -40\n   1 300   6\n", ret)

    def test_print2D2(self):
        a = BaseArray((2, 3), dtype=float, data=(2.55, -9.1, -4.45, 1.1, 300, 6))
        ret = a.__str__()
        self.assertEqual("  2.55  -9.1 -4.45\n   1.1   300     6\n", ret)

    def test_print3D(self):
        a = BaseArray((2, 3, 3), dtype=float,
                      data=(-5, -9, -40.5, 1, 300, 24, -10, -18, -80, 2, 600, 36, -15, -27, -120, 3, 900, 18))
        ret = a.__str__()
        self.assertEqual(
            "    -5    -9 -40.5\n     1   300    24\n\n   -10   -18   -80\n     2   600    36\n\n   -15   -27  -120\n     3   900    18\n",
            ret)

    def test_sort(self):
        a = BaseArray((2, 3), dtype=float, data=(-5, -9, -40, 1, 300, 6))
        a = a.sort(a, 0)
        b = BaseArray((2, 3), dtype=float, data=(-40, -9, -5, 1, 6, 300))
        self.assertEqual(b._BaseArray__data, a._BaseArray__data)

    def test_sort2(self):
        a = BaseArray((2, 3), dtype=float, data=(10, 5, 8, 0, 9, 0))
        a = a.sort(a, 0)
        b = BaseArray((2, 3), dtype=float, data=(5, 8, 10, 0, 0, 9))
        self.assertEqual(b._BaseArray__data, a._BaseArray__data)

    def test_sort3(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a = a.sort(a, 0)
        b = BaseArray((3, 3), dtype=float, data=(0, 1, 1, -2, 0, 4, 0, 1, 3))
        self.assertEqual(b._BaseArray__data, a._BaseArray__data)

    def test_sort4(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a = a.sort(a, 1)
        b = BaseArray((3, 3), dtype=float, data=(0, 0, -2, 1, 1, 0, 4, 3, 1))
        self.assertEqual(b._BaseArray__data, a._BaseArray__data)

    def test_sort5(self):
        a = BaseArray((4,), dtype=float, data=(5, 2, 6, 0))
        a = a.sort(a, 0)
        b = BaseArray((4,), dtype=float, data=(0, 2, 5, 6))
        self.assertEqual(b._BaseArray__data, a._BaseArray__data)

    def test_find1D(self):
        a = BaseArray((4,), dtype=float, data=(2, 4, 2, 9))
        results = a.search(2)
        self.assertEqual([0, 2], results)

    def test_find2D(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        results = a.search(2)
        self.assertEqual([[1, 0], [2, 1], [2, 2]], results)

    def test_find2D2(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 2.9, 0, 2, 0, 0, 0, 2.9, 2))
        results = a.search(2.9)
        self.assertEqual([[0, 1], [2, 1]], results)

    def test_find2D3(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 2.9, 0, 2, 0, 0, 0, 2.9, 2))
        results = a.search(3)
        self.assertEqual([], results)

    def test_find3D(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(0, 3, 3, 0, 0, 3, 0, 0))
        results = a.search(3)
        self.assertEqual([[0, 1, 0], [1, 0, 0], [0, 1, 1]], results)

    def test_find3D2(self):
        a = BaseArray((2, 2, 3), dtype=float, data=(0, 3, 3, 0, 0, 3, 0, 0, 3, 3, 0, 0))
        results = a.search(3)
        self.assertEqual([[0, 1, 0], [1, 0, 0], [0, 1, 1], [0, 0, 2], [0, 1, 2]], results)

    def test_scalar(self):
        a = BaseArray((2, 3), dtype=int, data=(-5, -9, -40, 1, 300, 6))
        a = 5 * a
        self.assertTrue([-25, -45, -200, 5, 1500, 30] == a._BaseArray__data and a.dtype == int)

    def test_scalar2(self):
        a = BaseArray((2, 3), dtype=int, data=(-5, -9, -40, 1, 300, 6))
        a = -5 * a
        self.assertTrue([25, 45, 200, -5, -1500, -30] == a._BaseArray__data and a.dtype == int)

    def test_scalar3(self):
        a = BaseArray((2, 3), dtype=int, data=(-5, -9, -40, 1, 300, 6))
        a = 5.5 * a
        self.assertTrue([-27.5, -49.5, -220, 5.5, 1650, 33] == a._BaseArray__data and a.dtype == float)

    def test_scalar4(self):
        a = BaseArray((2,), dtype=int, data=(-5, -9))
        a = 5.5 * a
        self.assertTrue([-27.5, -49.5] == a._BaseArray__data and a.dtype == float)

    def test_mmultiply(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        b = BaseArray((2, 2), dtype=int, data=(5, 2, 8, 7))
        c = (a * b)
        self.assertEqual([36, 22, 37, 30], c._BaseArray__data)

    def test_mmultiply2(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        b = BaseArray((2, 2), dtype=float, data=(5, 2.55, 8, 7))
        c = (a * b)
        self.assertEqual([36, 24.2, 37, 30.55], c._BaseArray__data)

    def test_mmultiply3(self):
        a = BaseArray((2, 3), dtype=float, data=(4.0, 2, 1, 4, 9, 0))
        b = BaseArray((3, 2), dtype=float, data=(5, 2.55, 8, 7, 6, 2))
        c = (a * b)
        self.assertEqual([42, 26.2, 92, 73.2], c._BaseArray__data)

    def test_mmultiply4cccc(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        b = BaseArray((2, 2), dtype=float, data=(5, 2.55, 8, 7))
        c = (a * b)
        self.assertTrue(c.dtype == float)

    def test_pluscccc(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        b = BaseArray((2, 2), dtype=float, data=(5, 2.55, 8, 7))
        c = (a + b)
        self.assertTrue(c.dtype == float)

    def test_minuscccc(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        b = BaseArray((2, 2), dtype=float, data=(5, 2.55, 8, 7))
        c = (a - b)
        self.assertTrue(c.dtype == float)

    def test_plus(self):
        a = BaseArray((2, 2), dtype=float, data=(2.5, 2.5, 1, 0))
        b = BaseArray((2, 2), dtype=float, data=(2.5, 2.5, 5, 0.5))
        c = a + b
        self.assertTrue([5.0, 5.0, 6.0, 0.5] == c._BaseArray__data and c.dtype == float)

    def test_plus2(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(2.5, 2.5, 1, 0, 1, 2, 3, 1))
        b = BaseArray((2, 2, 2), dtype=float, data=(2.5, 2.5, 5, 0.5, 1, 2, 3, 1))
        c = a + b
        self.assertEqual([5, 5, 6, 0.5, 2, 4, 6, 2], c._BaseArray__data)

    def test_minus(self):
        a = BaseArray((2, 2), dtype=float, data=(2.5, 2.5, 1, 0))
        b = BaseArray((2, 2), dtype=float, data=(2.5, 2.5, 5, 0.5))
        c = a - b
        self.assertEqual([0, 0, -4, -0.5], c._BaseArray__data)

    def test_minus2(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(2.5, 2.5, 1, 0, 1, 2, 3, 1))
        b = BaseArray((2, 2, 2), dtype=float, data=(2.5, 2.5, 5, 0.5, 1, 2, 3, 1))
        c = a - b
        self.assertEqual([0, 0, -4, -0.5, 0, 0, 0, 0], c._BaseArray__data)

    def test_log(self):
        a = BaseArray((2, 2), dtype=int, data=(16, 4, 32, 4))
        a = a.log_(2)
        self.assertEqual([4, 2, 5, 2], a._BaseArray__data)

    def test_exp(self):
        a = BaseArray((2, 2), dtype=int, data=(4, 2, 1, 4))
        a = a.exp_(2)
        self.assertEqual([16, 4, 1, 16], a._BaseArray__data)

    def test_log2(self):
        a = BaseArray((2, 2), dtype=int, data=(16, 4, 32, 4))
        a = a.log_(2)
        self.assertEqual(int, a.dtype)

    def test_log3(self):
        a = BaseArray((2, 2), dtype=int, data=(16, 4, 32, 4))
        a = a.log_(3)
        self.assertEqual(float, a.dtype)
